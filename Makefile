include ${SEPINC}/SEP.top
main=$(shell pwd)
FIG=>/dev/null out=
project1=${main}/circle_big
project2=${main}/circle_small
O=${main}/Obj
M=${main}/Mod
B=${main}/Bin
R=${main}/Fig
D=${main}/Dat


################################################################################

default: circle_big_run circle_small_run circle_big_figs circle_small_figs

circle_big_run:
	make --directory=circle_big circle_big

circle_small_run:
	make --directory=circle_small circle_small

circle_big_figs:
	make --directory=circle_big figs

circle_small_figs:
	make --directory=circle_small figs

circle_big_fwi_figs:
	make --directory=circle_big_FWI figs

approx-heaviside.H:
	make --directory=circle_big ${R}/approx-heaviside.v

################################################################################
clean: jclean

wipe:
	rm -f ${R}/*.pdf
	rm -f ${R}/*.ps
	rm -f ${R}/*.v*

burn:  clean wipe
	rm -f ${B}/*
	rm -f ${O}/*
	rm -f ${M}/*
	rm -f .make.dependencies.LINUX
	cd ${project1}; make burn
	cd ${project2}; make burn

################################################################################

include ${SEPINC}/SEP.bottom
